﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Menu : MonoBehaviour {

    GameObject exitButton;

    public void FixedUpdate()
    {
        exitButton = GameObject.Find("Exit_Button");
#if UNITY_WEBGL
        if (exitButton.activeSelf) exitButton.SetActive(false);
#endif
    }

    public void OnPlayButton()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void OnExitbutton()
    {
        Application.Quit();
    }
}