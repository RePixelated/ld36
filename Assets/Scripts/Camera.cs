﻿using UnityEngine;
using System.Collections;

public class Camera
{
    GameObject camera;
    Vector2 position;

    public Vector2 deadzone = new Vector2(10, 10);

    public Camera(GameObject obj)
    {
        camera = obj;
        position = camera.transform.position;
    }

    public void DeadzoneTrack(GameObject obj)
    {
        if (obj.transform.position.x < position.x - deadzone.x / 2)
            position.x = obj.transform.position.x + deadzone.x / 2;
        if (obj.transform.position.x > position.x + deadzone.x / 2)
            position.x = obj.transform.position.x - deadzone.x / 2;
        if (obj.transform.position.y < position.y - deadzone.y / 2)
            position.y = obj.transform.position.y + deadzone.y / 2;
        if (obj.transform.position.y > position.y + deadzone.y / 2)
            position.y = obj.transform.position.y - deadzone.y / 2;

        camera.transform.position = new Vector3(position.x, position.y, -10);
    }

    public void SmoothTrack(GameObject[] objects, bool pivotToCenter)
    {
        Vector2 position = new Vector2();

        foreach (GameObject obj in objects)
        {
            position.x += obj.transform.position.x;
            position.y += obj.transform.position.y;
        }

        position.x /= objects.Length + (pivotToCenter ? 3 : 0);
        position.y /= objects.Length + (pivotToCenter ? 3 : 0);

        camera.transform.position = new Vector3(position.x, position.y + 3.2f, -10);
    }
}