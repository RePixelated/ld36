﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    Rigidbody2D rigidb;

    float delayTime = 0.5f;
    bool oneshot = true;

    float despawnTime = 5;

    void Start()
    {
        rigidb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (delayTime > 0)
            delayTime -= Time.deltaTime;
        else if (oneshot)
        {
            if (gameObject.name == "Rock(Clone)" || gameObject.name == "Spear(Clone)")
                rigidb.AddForce(new Vector2(300, 100));
            else
                rigidb.AddForce(new Vector3(500, 50));

            rigidb.constraints = RigidbodyConstraints2D.None;
            GetComponent<SpriteRenderer>().enabled = true;

            oneshot = false;
        }


        if (despawnTime > 0)
            despawnTime -= Time.deltaTime;
        else
            Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (delayTime <= 0)
        {
            Destroy(gameObject);
        }
    }
}