﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

    private Character player;

    public void Start()
    {
        player = new Character(gameObject, new GameObject[0], new AudioClip[0], false);
    }

    public void FixedUpdate()
    {
        player.Control(1, 1);
    }
}