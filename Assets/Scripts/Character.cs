﻿using UnityEngine;
using System.Collections;

public class Character
{
    /// References
    GameObject character;
    AudioSource audioSource;
    Animator animator;
    Rigidbody2D rigidbody;
    AnimatorStateInfo animatorInfo;

    GameObject[] projectiles;
    AudioClip[] effects;

    /// Options
    float walkSpeed = 1.75f;
    float runSpeed = 5;
    float crouchWalkSpeed = 0.3f;
    float attackWalkSpeed = 0.2f;
    float jumpHeight = 10;

    /// State
    int health = 100;
    int losses = 0;
    bool doubleJump = false;

    int numPlayers;

    public Character(GameObject obj, GameObject[] projectiles, AudioClip[] effects, bool facingLeft)
    {
        character = obj;
        audioSource = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        animator = character.GetComponent<Animator>();
        rigidbody = character.GetComponent<Rigidbody2D>();

        this.projectiles = projectiles;
        this.effects = effects;

        if (!facingLeft) ChangeDirection("left");
    }


    public void Control(int id, int numPlayers)
    {
        Debug.Log(health);

        this.numPlayers = numPlayers;

        animatorInfo = animator.GetCurrentAnimatorStateInfo(0);

        /// Jumping
        if (ButtonManager("jump", id, numPlayers) && IsGrounded())
        {
            animator.SetTrigger("jump");
            PlaySound("jump");

            rigidbody.AddForce(Vector2.up * 2000 * jumpHeight * Time.deltaTime, ForceMode2D.Force);
        }
        /// Landing
        if (IsLanding() && !IsGrounded())
            animator.SetTrigger("landing");
        if (IsGrounded())
            animator.ResetTrigger("landing");

        /// Crouching
        if (ButtonManager("crouch", id, numPlayers))
        {
            animator.SetBool("crouch", true);
        }
        else animator.SetBool("crouch", false);

        /// Attack
        if (ButtonManager("attack", id, numPlayers) &&
            animatorInfo.tagHash != Animator.StringToHash("Attack"))
        {
            animator.SetTrigger("attack");

            /// Rock
            if (animator.GetInteger("weapon") == 0)
            {
                GameObject obj = GameObject.Instantiate(projectiles[0],
                    character.transform.position + new Vector3(1, 1.7f, 0), Quaternion.identity) as GameObject;
            }

            /// Spear
            if (animator.GetInteger("weapon") == 1)
            {
                GameObject.Instantiate(projectiles[1],
                    character.transform.position + new Vector3(1, 2f, 0), Quaternion.identity);
            }

            /// Bow
            if (animator.GetInteger("weapon") == 2)
            {
                GameObject.Instantiate(projectiles[2],
                    character.transform.position + new Vector3(0.5f, 1.5f, 0), Quaternion.identity);
            }

            /// Fire Bow
            if (animator.GetInteger("weapon") == 3)
            {
                GameObject.Instantiate(projectiles[2],
                    character.transform.position + new Vector3(0.5f, 1.5f, 0), Quaternion.identity);
            }

            /// Sword
            if (animator.GetInteger("weapon") == 4)
            {
                
            }

            /// Longsword
            if (animator.GetInteger("weapon") == 5)
            {
                
            }
        }

        /// Horizontal Movement
        if (AxisManager("horizontal", id, numPlayers) > 0 || AxisManager("horizontal", id, numPlayers) < 0)
        {
            animator.SetBool("move", true);

            /// Direction
            if (AxisManager("horizontal", id, numPlayers) > 0)
                ChangeDirection("right");
            if (AxisManager("horizontal", id, numPlayers) < 0)
                ChangeDirection("left");

            /// Ground
            if (animatorInfo.tagHash == Animator.StringToHash("Ground") ||
                animatorInfo.tagHash == Animator.StringToHash("Idle"))
            {
                animator.SetBool("ground", true);

                if (ButtonManager("modifier", id, numPlayers))
                {
                    animator.SetBool("modifier", true);
                    character.transform.position =
                        new Vector2(
                            character.transform.position.x + AxisManager("horizontal", id, numPlayers) * runSpeed * Time.deltaTime,
                            character.transform.position.y);
                }
                else
                {
                    animator.SetBool("modifier", false);

                    character.transform.position =
                        new Vector2(
                            character.transform.position.x + AxisManager("horizontal", id, numPlayers) * walkSpeed * Time.deltaTime,
                            character.transform.position.y);
                }
            }
            else animator.SetBool("ground", false);

            /// Crouch
            if (animatorInfo.tagHash == Animator.StringToHash("Crouch"))
            {
                character.transform.position =
                    new Vector2(
                        character.transform.position.x + AxisManager("horizontal", id, numPlayers) * crouchWalkSpeed * Time.deltaTime,
                        character.transform.position.y);
            }

            /// Airborne
            else if (animatorInfo.tagHash == Animator.StringToHash("Airborne"))
            {
                character.transform.position =
                    new Vector2(
                        character.transform.position.x + AxisManager("horizontal", id, numPlayers) * walkSpeed * Time.deltaTime,
                        character.transform.position.y);
            }

            /// Attack
            else if (animatorInfo.tagHash == Animator.StringToHash("Attack"))
            {
                character.transform.position =
                    new Vector2(
                        character.transform.position.x + AxisManager("horizontal", id, numPlayers) * attackWalkSpeed * Time.deltaTime,
                        character.transform.position.y);
            }
        }
        else animator.SetBool("move", false);

        if (animatorInfo.tagHash == Animator.StringToHash("Ground") ||
                animatorInfo.tagHash == Animator.StringToHash("Idle") ||
                rigidbody.velocity.y == 0)
        {
            animator.SetBool("ground", true);
        }
        else animator.SetBool("ground", false);


        if (Physics2D.BoxCast(character.transform.position + new Vector3(0, 1, 0),
            new Vector2(1, 2), 0, Vector2.right).collider.tag == "Weapon")
        {
            animator.SetTrigger("hit");
            PlaySound("hit");

            health -= 5;

            if (health <= 0)
            {
                health = 100;
                losses++;
            }
        }
    }

    bool ButtonManager(string action, int id, int numPlayers)
    {
        /// Modifier
        if (action == "modifier")
        {
            if (numPlayers == 1) { }
            else if (numPlayers == 2)
            {
                if (id == 1)
                    return Input.GetButton("2Keyboard_Player1_Modifier");
                else if (id == 2)
                    return Input.GetButton("2Keyboard_Player2_Modifier");
            }
        }

        /// Jump
        else if (action == "jump")
        {
            if (numPlayers == 1) { }
            else if (numPlayers == 2)
            {
                if (id == 1)
                    return Input.GetButtonDown("2Keyboard_Player1_Jump");
                else if (id == 2)
                    return Input.GetButtonDown("2Keyboard_Player2_Jump");
            }
        }

        /// Crouch
        else if (action == "crouch")
        {
            if (numPlayers == 1) { }
            else if (numPlayers == 2)
            {
                if (id == 1)
                    return Input.GetButton("2Keyboard_Player1_Crouch");
                else if (id == 2)
                    return Input.GetButton("2Keyboard_Player2_Crouch");
            }
        }

        /// Attack
        else if (action == "attack")
        {
            if (numPlayers == 1) { }
            else if (numPlayers == 2)
            {
                if (id == 1)
                    return Input.GetButtonDown("2Keyboard_Player1_Attack");
                else if (id == 2)
                    return Input.GetButtonDown("2Keyboard_Player2_Attack");
            }
        }

        return false;
    }
    float AxisManager(string action, int id, int numPlayers)
    {
        /// Horizontal
        if (action == "horizontal")
        {
            if (numPlayers == 1) { }
            else if (numPlayers == 2)
            {
                if (id == 1)
                    return Input.GetAxis("2Keyboard_Player1_Horizontal");
                else if (id == 2)
                    return Input.GetAxis("2Keyboard_Player2_Horizontal");
            }
        }

        return 0;
    }

    public void WeaponUpgrade()
    {
        animator.SetInteger("weapon", animator.GetInteger("weapon") + 1);
        PlaySound("powerup");
    }

    void PlaySound(string sound)
    {
        if (sound == "jump")
            audioSource.clip = effects[0];
        else if (sound == "hit")
            audioSource.clip = effects[1];
        else if (sound == "powerup")
            audioSource.clip = effects[2];

        audioSource.Play();
    }

    void ChangeDirection(string direction)
    {
        if ((animator.GetBool("facingLeft") && direction == "right") ||
            (!animator.GetBool("facingLeft") && direction == "left"))
        {
            character.transform.Rotate(0, direction == "right" ? 180 : -180, 0);
            animator.SetBool("facingLeft", !animator.GetBool("facingLeft"));
        }
    }

    bool IsGrounded()
    {
        return rigidbody.velocity.y == 0;
    }
    bool IsLanding()
    {
        /// Hardcoded ground level: 0
        return character.transform.position.y < 1.5f &&
            character.transform.position.y > 0 &&
            rigidbody.velocity.y < 0;
    }

    public int GetHealth()
    {
        return health;
    }
    public int GetLosses()
    {
        return losses;
    }
}