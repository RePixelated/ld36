﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Master : MonoBehaviour {

    public GameObject rockProjectile;
    public GameObject spearProjectile;
    public GameObject arrowProjectile;

    public AudioClip jumpClip;
    public AudioClip hitClip;
    public AudioClip powerupClip;

    private Character[] players;
    private GameObject[] characters;
    private Camera cam;

    float weaponTime = 30;
    int weaponLevel = 0;
    Text weaponUpgradeTimer;
    RectTransform healthbarLeft;
    RectTransform healthbarRight;
    Text lossCounterLeft;
    Text lossCounterRight;

    int numPlayers;

    public void Start()
    {
        cam = new Camera(GameObject.FindGameObjectWithTag("MainCamera"));

        weaponUpgradeTimer = GameObject.Find("WeaponUpgradeTimer").GetComponent<Text>();
        healthbarLeft = GameObject.Find("HealthbarLeftBar").GetComponent<RectTransform>();
        healthbarRight = GameObject.Find("HealthbarRightBar").GetComponent<RectTransform>();
        lossCounterLeft = GameObject.Find("LossCounterLeft").GetComponent<Text>();
        lossCounterRight = GameObject.Find("LossCounterRight").GetComponent<Text>();

        Init();
    }

    //public void Init(int numPlayers)
    void Init()
    {
        /// Nearly working code-based Init script
        /*this.numPlayers = numPlayers;

        for (int i = 0; i < numPlayers; i++)
        {
            Debug.Log(i);

            characters = new GameObject[numPlayers];

            characters[i] = new GameObject("Player" + i);
            characters[i].transform.position = new Vector2(
                0, (numPlayers == 1 ? 0 : (i == 1 ? -5 : 5)));
            characters[i].tag = "Player";

            characters[i].AddComponent<SpriteRenderer>();
            characters[i].AddComponent<Animator>().runtimeAnimatorController = Resources.Load("Animations/" +
                (i == 0 ? "Caveman" : "CavemanAlt")) as RuntimeAnimatorController;
            characters[i].AddComponent<BoxCollider2D>();
            characters[i].AddComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

            players[i] = new Character(characters[i]);
        }*/

        /// Temporary, hardcoded Init script
        players = new Character[2];
        characters = new GameObject[2];

        characters[0] = GameObject.Find("Caveman");
        characters[1] = GameObject.Find("CavemanAlt");

        players[0] = new Character(characters[0],
            new GameObject[] { rockProjectile, spearProjectile, arrowProjectile },
            new AudioClip[] { jumpClip, hitClip, powerupClip }, true);
        players[1] = new Character(characters[1],
            new GameObject[] { rockProjectile, spearProjectile, arrowProjectile },
            new AudioClip[] { jumpClip, hitClip, powerupClip }, false);
    }

    public void FixedUpdate()
    {
        for (int i = 0; i < 2; i++)
            players[i].Control(i + 1, 2);

        cam.SmoothTrack(characters, true);

        WeaponManager();

        GUI();
    }

    void WeaponManager()
    {
        weaponTime -= Time.deltaTime;

        /// Time to upgrade
        if (weaponTime <= 0 && weaponLevel < 5)
        {
            for (int i = 0; i < 2; i++)
                players[i].WeaponUpgrade();

            weaponTime = 30;
            weaponLevel++;
        }

        /// GUI
        weaponUpgradeTimer.text = ((int)weaponTime).ToString();
    }

    void GUI()
    {
        healthbarLeft.localScale = new Vector3(players[0].GetHealth() / 100, 1, 1);
        healthbarRight.localScale = new Vector3((float)players[1].GetHealth() / 100, 1, 1);

        lossCounterLeft.text = (players[0].GetLosses()).ToString();
        lossCounterRight.text = (players[1].GetLosses()).ToString();
    }
}
